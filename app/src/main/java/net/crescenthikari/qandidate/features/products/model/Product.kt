package net.crescenthikari.qandidate.features.products.model

data class Product(
        val id: String,
        val name: String,
        val stock: Int,
        val price: Long,
        val thumbnailUrl: String,
        val imageUrl: String,
        val description: String
)