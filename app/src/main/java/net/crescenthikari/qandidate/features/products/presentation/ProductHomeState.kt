package net.crescenthikari.qandidate.features.products.presentation

import net.crescenthikari.qandidate.core.platform.state.ViewState
import net.crescenthikari.qandidate.features.products.ProductHomeView

data class ProductHomeState(
        val productHomeView: ProductHomeView
) : ViewState.FeatureState()