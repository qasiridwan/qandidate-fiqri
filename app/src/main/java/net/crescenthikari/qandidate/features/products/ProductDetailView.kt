package net.crescenthikari.qandidate.features.products

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductDetailView(
        val id: String,
        val name: String,
        val stock: Int,
        val price: Long,
        val imageUrl: String,
        val description: String
) : Parcelable