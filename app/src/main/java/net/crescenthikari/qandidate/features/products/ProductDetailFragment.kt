package net.crescenthikari.qandidate.features.products

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import net.crescenthikari.qandidate.R
import net.crescenthikari.qandidate.core.extensions.formatCurrency
import net.crescenthikari.qandidate.core.extensions.toHtmlSpannable

/**
 * A fragment representing a single [ProductDetailView] detail screen.
 * This fragment is either contained in a [ProductHomeActivity]
 * in two-pane mode (on tablets) or a [ProductDetailActivity]
 * on handsets.
 */
class ProductDetailFragment : Fragment() {

    companion object {
        private const val KEY_PRODUCT_DETAIL = "KEY_PRODUCT_DETAIL"
        fun newInstance(productDetail: ProductDetailView): ProductDetailFragment {
            return ProductDetailFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(KEY_PRODUCT_DETAIL, productDetail)
                }
            }
        }
    }

    private lateinit var productImage: ImageView
    private lateinit var productName: TextView
    private lateinit var productPrice: TextView
    private lateinit var productDescription: TextView

    private lateinit var productDetail: ProductDetailView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let { bundle ->
            if (bundle.containsKey(KEY_PRODUCT_DETAIL)) {
                productDetail = bundle.getParcelable(KEY_PRODUCT_DETAIL)
                        ?: throw RuntimeException("Product Detail data not found")
            }
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.product_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        productImage = view.findViewById(R.id.product_detail_image)
        productName = view.findViewById(R.id.product_detail_name)
        productPrice = view.findViewById(R.id.product_detail_price)
        productDescription = view.findViewById(R.id.product_detail_description)

        context?.run {
            Glide.with(this)
                    .load(productDetail.imageUrl)
                    .into(productImage)
        }
        productName.text = productDetail.name
        productPrice.text = productDetail.price.formatCurrency()
        productDescription.text = productDetail.description.toHtmlSpannable()

    }
}
