package net.crescenthikari.qandidate.features.products.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.SingleObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import net.crescenthikari.qandidate.core.exception.Failure
import net.crescenthikari.qandidate.core.interactor.UseCase
import net.crescenthikari.qandidate.core.platform.state.ViewState
import net.crescenthikari.qandidate.core.schedulers.CoreSchedulers
import net.crescenthikari.qandidate.features.products.ProductHomeView
import net.crescenthikari.qandidate.features.products.ProductView
import net.crescenthikari.qandidate.features.products.interactor.GetProductHome
import net.crescenthikari.qandidate.features.products.model.Product
import net.crescenthikari.qandidate.features.products.model.ProductHome
import javax.inject.Inject

class ProductHomeViewModel @Inject constructor(
        private val getProductHome: GetProductHome,
        private val schedulers: CoreSchedulers
) : ViewModel() {
    private val disposables: CompositeDisposable = CompositeDisposable()
    private lateinit var productHomeState: MutableLiveData<ViewState>

    private fun loadProductHome() {
        getProductHome(UseCase.None())
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.main())
                .subscribe(object : SingleObserver<ProductHome> {
                    override fun onSuccess(productHome: ProductHome) {
                        productHomeState.value = ProductHomeState(productHome.mapToProductHomeView())
                    }

                    override fun onSubscribe(d: Disposable) {
                        disposables.add(d)
                    }

                    override fun onError(e: Throwable) {
                        productHomeState.value = ViewState.FailureState(Failure.NetworkConnection)
                    }
                })
    }

    private fun Product.mapToProductView(): ProductView {
        return ProductView(id, name, stock, price, thumbnailUrl, imageUrl, description)
    }

    private fun ProductHome.mapToProductHomeView(): ProductHomeView {
        return ProductHomeView(banner.image, products.map { it.mapToProductView() })
    }

    fun observeProductHomeState(): LiveData<ViewState> {
        if (!this::productHomeState.isInitialized) {
            productHomeState = MutableLiveData()
            productHomeState.value = ViewState.LoadingState
            loadProductHome()
        }
        return productHomeState
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}