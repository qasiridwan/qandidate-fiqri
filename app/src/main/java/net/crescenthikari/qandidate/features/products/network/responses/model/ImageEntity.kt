package net.crescenthikari.qandidate.features.products.network.responses.model

import com.google.gson.annotations.SerializedName

data class ImageEntity(
        @SerializedName("thumbnail") val thumbnail: String?,
        @SerializedName("large") val large: String?
)