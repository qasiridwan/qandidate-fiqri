package net.crescenthikari.qandidate.features.products.network.responses.model

import com.google.gson.annotations.SerializedName

data class BannerEntity(
        @SerializedName("image") val image: String?
)