package net.crescenthikari.qandidate.features.products.adapter

import android.content.Context
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import net.crescenthikari.qandidate.R
import net.crescenthikari.qandidate.core.extensions.formatCurrency
import net.crescenthikari.qandidate.core.platform.viewholder.BaseViewHolder
import net.crescenthikari.qandidate.core.platform.viewholder.HasBinding
import net.crescenthikari.qandidate.features.products.ProductView

class ProductViewHolder(
        parent: ViewGroup
) : BaseViewHolder(parent, R.layout.product_list_content), HasBinding<ProductView> {
    private val context: Context
        get() = itemView.context
    private val name: TextView = itemView.findViewById(R.id.product_item_name)
    private val price: TextView = itemView.findViewById(R.id.product_item_price)
    private val image: ImageView = itemView.findViewById(R.id.product_item_image)
    private val stock: TextView = itemView.findViewById(R.id.product_item_stock)

    override fun bind(data: ProductView) {
        name.text = data.name
        price.text = data.price.formatCurrency()
        stock.text = data.stock.toString()
        Glide.with(context)
                .load(data.thumbnailUrl)
                .into(image)
    }
}