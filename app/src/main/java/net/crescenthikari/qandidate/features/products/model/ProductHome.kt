package net.crescenthikari.qandidate.features.products.model

data class ProductHome(
        val banner: Banner,
        val products: List<Product>
)