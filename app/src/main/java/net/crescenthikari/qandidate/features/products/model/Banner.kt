package net.crescenthikari.qandidate.features.products.model

data class Banner(val image: String)