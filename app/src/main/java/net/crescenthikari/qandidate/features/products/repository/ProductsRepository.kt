package net.crescenthikari.qandidate.features.products.repository

import io.reactivex.Single
import net.crescenthikari.qandidate.features.products.model.ProductHome

interface ProductsRepository {
    fun getProductHome(): Single<ProductHome>
}