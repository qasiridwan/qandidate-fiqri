package net.crescenthikari.qandidate.features.products

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import dagger.android.AndroidInjection
import net.crescenthikari.qandidate.R
import net.crescenthikari.qandidate.core.platform.state.ViewState
import net.crescenthikari.qandidate.di.modules.viewmodels.ViewModelFactory
import net.crescenthikari.qandidate.features.products.adapter.ProductAdapter
import net.crescenthikari.qandidate.features.products.adapter.ProductClickListener
import net.crescenthikari.qandidate.features.products.presentation.ProductHomeState
import net.crescenthikari.qandidate.features.products.presentation.ProductHomeViewModel
import timber.log.Timber
import javax.inject.Inject

/**
 * An activity representing a list of Pings. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a [ProductDetailActivity] representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
class ProductHomeActivity : AppCompatActivity() {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var twoPane: Boolean = false
    private lateinit var productAdapter: ProductAdapter
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var productHomeViewModel: ProductHomeViewModel

    private val toolbar: Toolbar by lazy(LazyThreadSafetyMode.NONE) {
        findViewById<Toolbar>(R.id.toolbar)
    }

    private val productList: RecyclerView by lazy(LazyThreadSafetyMode.NONE) {
        findViewById<RecyclerView>(R.id.product_list)
    }

    private val progressBar: ProgressBar by lazy(LazyThreadSafetyMode.NONE) {
        findViewById<ProgressBar>(R.id.product_home_progressbar)
    }

    private val bannerImage: ImageView by lazy(LazyThreadSafetyMode.NONE) {
        findViewById<ImageView>(R.id.product_banner)
    }

    private val detailContainer: View? by lazy(LazyThreadSafetyMode.NONE) {
        findViewById<View?>(R.id.item_detail_container)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_home)
        productHomeViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ProductHomeViewModel::class.java)

        setSupportActionBar(toolbar)
        toolbar.title = title

        if (detailContainer != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true
        }

        setupRecyclerView(productList)

        productHomeViewModel.observeProductHomeState()
                .observe(this, Observer { state ->
                    renderViewState(state)
                })
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        productAdapter = ProductAdapter(object : ProductClickListener {
            override fun onProductClicked(product: ProductView) {
                openProductDetail(product)
            }
        })
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = productAdapter
    }

    private fun renderViewState(state: ViewState) {
        when (state) {
            is ViewState.LoadingState -> renderLoadingState()
            is ProductHomeState -> renderProductHome(state)
            is ViewState.FailureState -> renderFailureState(state)
        }
    }

    private fun renderLoadingState() {
        showLoadingProgress()
    }

    private fun renderProductHome(state: ProductHomeState) {
        hideLoadingProgress()
        showBannerImage(state.productHomeView.bannerImage)
        productAdapter.submitList(state.productHomeView.products)
    }

    private fun showBannerImage(imageUrl: String) {
        Glide.with(this)
                .load(imageUrl)
                .into(bannerImage)
    }

    private fun renderFailureState(failureState: ViewState.FailureState) {
        hideLoadingProgress()
        Timber.e(failureState.failure.toString())
    }

    private fun showLoadingProgress() {
        progressBar.visibility = View.VISIBLE
    }

    private fun hideLoadingProgress() {
        progressBar.visibility = View.GONE
    }

    private fun openProductDetail(product: ProductView) {
        // open product detail here
        if (twoPane) {
            val fragment = ProductDetailFragment.newInstance(product.mapToProductDetailView())
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit()
        } else {
            ProductDetailActivity
                    .getProductDetailIntent(this, product.mapToProductDetailView())
                    .run { startActivity(this) }
        }
    }

    private fun ProductView.mapToProductDetailView(): ProductDetailView {
        return ProductDetailView(
                id, name, stock, price, imageUrl, description
        )
    }
}
