package net.crescenthikari.qandidate.features.products.repository

import io.reactivex.Single
import net.crescenthikari.qandidate.features.products.model.Banner
import net.crescenthikari.qandidate.features.products.model.Product
import net.crescenthikari.qandidate.features.products.model.ProductHome
import net.crescenthikari.qandidate.features.products.network.ProductsApi
import net.crescenthikari.qandidate.features.products.network.responses.model.BannerEntity
import net.crescenthikari.qandidate.features.products.network.responses.model.ProductEntity
import net.crescenthikari.qandidate.features.products.network.responses.model.ProductsEntity
import javax.inject.Inject

class ProductsNetworkRepository @Inject constructor(
        private val productsApi: ProductsApi
) : ProductsRepository {
    override fun getProductHome(): Single<ProductHome> {
        return productsApi.products()
                .map { response ->
                    response.data?.mapToProductHome() ?: throw Exception("invalid data returned")
                }
    }

    private fun ProductsEntity.mapToProductHome(): ProductHome {
        return ProductHome(
                banner.mapToBanner(),
                products?.map { productEntity -> productEntity.mapToProduct() } ?: emptyList()
        )
    }

    private fun BannerEntity?.mapToBanner(): Banner {
        return Banner(this?.image ?: "")
    }

    private fun ProductEntity?.mapToProduct(): Product {
        return Product(
                this?.id?.toString() ?: "",
                this?.name ?: "",
                this?.stock ?: 0,
                this?.price ?: 0,
                this?.images?.thumbnail ?: "",
                this?.images?.large ?: "",
                this?.description ?: ""
        )
    }
}