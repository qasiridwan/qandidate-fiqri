package net.crescenthikari.qandidate.features.products

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductView(
        val id: String,
        val name: String,
        val stock: Int,
        val price: Long,
        val thumbnailUrl: String,
        val imageUrl: String,
        val description: String
) : Parcelable