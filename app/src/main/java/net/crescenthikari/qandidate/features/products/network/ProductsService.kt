package net.crescenthikari.qandidate.features.products.network

import io.reactivex.Single
import net.crescenthikari.qandidate.features.products.network.responses.ProductsResponse
import retrofit2.Retrofit
import javax.inject.Inject

class ProductsService @Inject constructor(
        retrofit: Retrofit
) : ProductsApi {
    private val productsApi: ProductsApi by lazy {
        retrofit.create(ProductsApi::class.java)
    }

    override fun products(): Single<ProductsResponse> = productsApi.products()
}