package net.crescenthikari.qandidate.features.products.network.responses.model

import com.google.gson.annotations.SerializedName

data class ProductsEntity(
        @SerializedName("banner") val banner: BannerEntity?,
        @SerializedName("products") val products: List<ProductEntity>?
)