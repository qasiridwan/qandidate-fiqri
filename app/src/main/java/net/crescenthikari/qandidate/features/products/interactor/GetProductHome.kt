package net.crescenthikari.qandidate.features.products.interactor

import io.reactivex.Single
import net.crescenthikari.qandidate.core.interactor.SingleUseCase
import net.crescenthikari.qandidate.core.interactor.UseCase.None
import net.crescenthikari.qandidate.features.products.model.ProductHome
import net.crescenthikari.qandidate.features.products.repository.ProductsRepository
import javax.inject.Inject

class GetProductHome @Inject constructor(
        private val productsRepository: ProductsRepository
) : SingleUseCase<ProductHome, None>() {
    override fun invoke(params: None): Single<ProductHome> {
        return productsRepository.getProductHome()
    }
}