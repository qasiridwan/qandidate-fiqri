package net.crescenthikari.qandidate.features.products.adapter

import net.crescenthikari.qandidate.features.products.ProductView

interface ProductClickListener {
    fun onProductClicked(product: ProductView)
}