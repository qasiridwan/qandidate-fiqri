package net.crescenthikari.qandidate.features.products

data class ProductHomeView(
        val bannerImage: String,
        val products: List<ProductView>
)