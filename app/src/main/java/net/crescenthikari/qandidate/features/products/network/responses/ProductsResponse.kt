package net.crescenthikari.qandidate.features.products.network.responses

import com.google.gson.annotations.SerializedName
import net.crescenthikari.qandidate.features.products.network.responses.model.ProductsEntity

data class ProductsResponse(
        @SerializedName("status") val status: String?,
        @SerializedName("message") val message: String?,
        @SerializedName("data") val data: ProductsEntity?
)