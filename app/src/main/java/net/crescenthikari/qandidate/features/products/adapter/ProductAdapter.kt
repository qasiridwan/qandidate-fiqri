package net.crescenthikari.qandidate.features.products.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import net.crescenthikari.qandidate.features.products.ProductView

class ProductAdapter(
        private val productClickListener: ProductClickListener,
        diffCallback: DiffUtil.ItemCallback<ProductView> = DEFAULT_DIFF
) : ListAdapter<ProductView, ProductViewHolder>(diffCallback) {
    companion object {
        private val DEFAULT_DIFF = object : DiffUtil.ItemCallback<ProductView>() {
            override fun areItemsTheSame(oldItem: ProductView, newItem: ProductView): Boolean {
                return oldItem.id == newItem.id

            }

            override fun areContentsTheSame(oldItem: ProductView, newItem: ProductView): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(parent).apply {
            itemView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    productClickListener.onProductClicked(getItem(adapterPosition))
                }
            }
        }
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}