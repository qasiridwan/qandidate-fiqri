package net.crescenthikari.qandidate.features.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import net.crescenthikari.qandidate.R
import net.crescenthikari.qandidate.features.products.ProductHomeActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Intent(this, ProductHomeActivity::class.java).run {
            startActivity(this)
            finish()
        }
    }
}
