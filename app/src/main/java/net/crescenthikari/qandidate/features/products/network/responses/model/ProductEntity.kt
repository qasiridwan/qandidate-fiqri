package net.crescenthikari.qandidate.features.products.network.responses.model

import com.google.gson.annotations.SerializedName

data class ProductEntity(
        @SerializedName("product_id") val id: Int?,
        @SerializedName("product_name") val name: String?,
        @SerializedName("price") val price: Long?,
        @SerializedName("stock") val stock: Int?,
        @SerializedName("description") val description: String?,
        @SerializedName("images") val images: ImageEntity?
)