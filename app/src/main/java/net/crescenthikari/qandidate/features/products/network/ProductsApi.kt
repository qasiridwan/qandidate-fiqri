package net.crescenthikari.qandidate.features.products.network

import io.reactivex.Single
import net.crescenthikari.qandidate.features.products.network.responses.ProductsResponse
import retrofit2.http.GET

interface ProductsApi {
    @GET("gsq5w")
    fun products(): Single<ProductsResponse>
}