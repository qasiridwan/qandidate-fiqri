package net.crescenthikari.qandidate.di.modules

import dagger.Module
import dagger.Provides
import net.crescenthikari.qandidate.di.ApplicationScope
import net.crescenthikari.qandidate.features.products.repository.ProductsNetworkRepository
import net.crescenthikari.qandidate.features.products.repository.ProductsRepository

@Module
class RepositoryModule {
    @Provides
    @ApplicationScope
    fun provideProductsRepository(repository: ProductsNetworkRepository): ProductsRepository {
        return repository
    }
}