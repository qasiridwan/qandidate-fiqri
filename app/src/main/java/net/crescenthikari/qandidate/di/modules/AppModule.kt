package net.crescenthikari.qandidate.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import net.crescenthikari.qandidate.QandidateApp
import net.crescenthikari.qandidate.core.schedulers.CoreSchedulers
import net.crescenthikari.qandidate.core.schedulers.DefaultSchedulers
import net.crescenthikari.qandidate.di.ApplicationScope
import javax.inject.Named

@Module
class AppModule {
    @Provides
    @ApplicationScope
    @Named("appContext")
    internal fun provideAppContext(app: QandidateApp): Context {
        return app
    }

    @Provides
    @ApplicationScope
    internal fun provideDefaultSchedulers(defaultSchedulers: DefaultSchedulers): CoreSchedulers {
        return defaultSchedulers
    }
}