package net.crescenthikari.qandidate.di.components

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import net.crescenthikari.qandidate.QandidateApp
import net.crescenthikari.qandidate.di.ApplicationScope
import net.crescenthikari.qandidate.di.modules.AppModule
import net.crescenthikari.qandidate.di.modules.NetworkModule
import net.crescenthikari.qandidate.di.modules.RepositoryModule
import net.crescenthikari.qandidate.di.modules.activities.ActivityBuilder
import net.crescenthikari.qandidate.di.modules.viewmodels.ViewModelModule

@ApplicationScope
@Component(modules = [
    AndroidInjectionModule::class,
    AppModule::class,
    NetworkModule::class,
    RepositoryModule::class,
    ActivityBuilder::class,
    ViewModelModule::class
])
interface AppComponent : AndroidInjector<QandidateApp> {

    @Component.Builder
    abstract class Builder {
        @BindsInstance
        abstract fun app(app: QandidateApp): Builder

        abstract fun build(): AppComponent
    }
}