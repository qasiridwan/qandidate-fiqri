package net.crescenthikari.qandidate.di.modules

import dagger.Module
import dagger.Provides
import net.crescenthikari.qandidate.di.ApplicationScope
import net.crescenthikari.qandidate.features.products.network.ProductsApi
import net.crescenthikari.qandidate.features.products.network.ProductsService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named

@Module
class NetworkModule {
    @Provides
    @Named("baseUrl")
    @ApplicationScope
    internal fun provideBaseUrl(): String {
        return "https://api.myjson.com/bins/"
    }

    @ApplicationScope
    @Provides
    internal fun provideHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()
    }

    @Provides
    @ApplicationScope
    internal fun provideRetrofit(
            @Named("baseUrl") baseUrl: String,
            okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    @Provides
    @ApplicationScope
    internal fun provideProductsApi(productsService: ProductsService): ProductsApi {
        return productsService
    }
}