package net.crescenthikari.qandidate.di.modules.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import net.crescenthikari.qandidate.features.products.presentation.ProductHomeViewModel

@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ProductHomeViewModel::class)
    abstract fun bindsMoviesViewModel(productHomeViewModel: ProductHomeViewModel): ViewModel
}