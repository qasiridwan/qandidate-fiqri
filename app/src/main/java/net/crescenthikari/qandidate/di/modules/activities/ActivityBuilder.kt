package net.crescenthikari.qandidate.di.modules.activities

import dagger.Module
import dagger.android.ContributesAndroidInjector
import net.crescenthikari.qandidate.di.ActivityScope
import net.crescenthikari.qandidate.features.products.ProductHomeActivity

@Module
abstract class ActivityBuilder {
    @ActivityScope
    @ContributesAndroidInjector
    abstract fun provideProductHomeActivity(): ProductHomeActivity
}