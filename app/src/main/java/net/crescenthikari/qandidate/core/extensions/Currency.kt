package net.crescenthikari.qandidate.core.extensions

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

fun Number.formatCurrency(
        prefix: String = "Rp ",
        postfix: String = "",
        locale: Locale = Locale("id")
): String {
    val numberFormatter = DecimalFormat("#,###", DecimalFormatSymbols.getInstance(locale))
    return prefix + numberFormatter.format(this) + postfix
}