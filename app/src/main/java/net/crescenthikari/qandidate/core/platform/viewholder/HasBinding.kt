package net.crescenthikari.qandidate.core.platform.viewholder

interface HasBinding<T> {
    fun bind(data: T)
}