package net.crescenthikari.qandidate.core.interactor

import io.reactivex.Single

abstract class UseCase {
    class None
}

abstract class SingleUseCase<Type, Params> where Type : Any {
    abstract operator fun invoke(params: Params): Single<Type>
}