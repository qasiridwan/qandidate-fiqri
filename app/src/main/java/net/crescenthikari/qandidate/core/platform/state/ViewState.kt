package net.crescenthikari.qandidate.core.platform.state

import net.crescenthikari.qandidate.core.exception.Failure

sealed class ViewState {
    object LoadingState : ViewState()
    data class FailureState(val failure: Failure) : ViewState()

    /** feature specific state can extend this [FeatureState] class */
    abstract class FeatureState : ViewState()
}