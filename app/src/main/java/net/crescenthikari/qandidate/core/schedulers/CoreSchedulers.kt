package net.crescenthikari.qandidate.core.schedulers

import io.reactivex.Scheduler

interface CoreSchedulers {
    fun io(): Scheduler
    fun main(): Scheduler
}