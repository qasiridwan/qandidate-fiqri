package net.crescenthikari.qandidate.core.extensions

import org.junit.Assert.assertEquals
import org.junit.Test

class CurrencyTest {

    @Test
    fun formatCurrencyShouldReturnValidFormattedThousandNumber() {
        // given
        val number = 1000
        val formatted = "Rp 1.000"

        // when
        val result = number.formatCurrency()

        // then
        assertEquals(formatted, result)
    }

    @Test
    fun formatCurrencyShouldReturnValidFormattedMillionNumber() {
        // given
        val number = 1000000
        val formatted = "Rp 1.000.000"

        // when
        val result = number.formatCurrency()

        // then
        assertEquals(formatted, result)
    }
}